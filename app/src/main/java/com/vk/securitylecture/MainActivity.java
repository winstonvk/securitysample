package com.vk.securitylecture;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText login;
    private EditText password;

    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login = findViewById(R.id.login);
        password = findViewById(R.id.password);

        DBHelper dbHelper = new DBHelper(this);
        db = dbHelper.getWritableDatabase();
        addUserToDb();
    }

    public void buttonClick(View view) {
        boolean abc = checkLogin(login.getText().toString(), password.getText().toString());
        Toast.makeText(this, "abc = " + abc, Toast.LENGTH_SHORT).show();
    }

    private void addUserToDb() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("USERNAME", "LOX");
        contentValues.put("PASSWORD", "LOX");
        db.insert("LOGINS", null, contentValues);
    }

    private boolean checkLogin(String login, String password) {
        boolean bool = false;

        Cursor cursor = db.rawQuery(
                "select * from LOGINS where USERNAME = ? and PASSWORD = ?", new String[]{login, password}
        );

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                bool = true;
            }
            cursor.close();
        }
        return bool;
    }
}
