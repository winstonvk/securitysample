package com.vk.securitylecture;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author Vladimir Kokhanov
 */
class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, "test_db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table LOGINS ("
                + "USERNAME text primary key,"
                + "PASSWORD text" + ");"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // empty
    }
}
